import image1 from '../image/1.jpg';
import image2 from '../image/2.jpg';
import image3 from '../image/3.jpg';
import image4 from '../image/4.jpg';
import image5 from '../image/5.jpg';
import image6 from '../image/6.jpg';
import image7 from '../image/7.jpg';
import image8 from '../image/8.jpg';
import image9 from '../image/9.jpg';

export default class dataService {
    _DATA = {
        list: [{
            id: 1,
            title: 'My little humster',
            description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.',
            img: image1,
        }, {
            id: 2,
            title: 'Запись 2',
            description: '2 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image2,
        }, {
            id: 3,
            title: 'Запись 3',
            description: '3 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image3,
        }, {
            id: 4,
            title: 'Запись 4',
            description: '4 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image4,
        }, {
            id: 5,
            title: 'Запись 5',
            description: '5 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image5,
        }, {
            id: 6,
            title: 'Запись 6',
            description: '6 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image6,
        }, {
            id: 7,
            title: 'Запись 7',
            description: '7 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image7,
        }, {
            id: 8,
            title: 'Запись 8',
            description: '8 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image8,
        }, {
            id: 9,
            title: 'Запись 9',
            description: '9 There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.',
            img: image9,
        }]
    };

    getAllRecords() {
        return new Promise((resolve) => {
            const records = this._DATA.list.map(this._initRecord);
            resolve(records);
        });
    }

    deleteRecordByIndex(index) {
        return new Promise((resolve) => {
            this._DATA.list.splice(index, 1);

            const records = this._DATA.list.map(this._initRecord);
            resolve(records);
        });
    }

    getRecord (id) {
        return new Promise((resolve) => {
            resolve(this._initRecord(this._DATA.list.find((rec) => rec.id === id)));
        });
    }

    _initRecord (rawRecord) {
        return {
            id: rawRecord.id,
            title: rawRecord.title,
            description: rawRecord.description,
            img: rawRecord.img,
        }
    }
}