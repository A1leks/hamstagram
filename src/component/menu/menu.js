import React from 'react';

import './menu.css';

const Content = (props) => {
    const renderMenuItems = () => {
        const { records, activeId, onActivate } = props;

        return records.map((item) => {
            return (
                <li key={item.id}
                    className={item.id === activeId ? 'active':''}
                    onClick={() => onActivate(item)}>
                    {item.title}
                </li>
            );
        })
    };

    return (
        <div className="menu">
            <h2 className="menu-title">Your images</h2>
            <ul className="menu-items">
                {renderMenuItems()}
            </ul>
        </div>
    );
};

export default Content;