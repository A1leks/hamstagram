import React, { Component } from 'react';
import Record from "../record/record";
import Menu from "../menu/menu";
import ControlsBar from "../controls-bar/controls-bar";

import './content.css';
import DataService from "../../service/data-service";

export default class Content extends Component {

    dataService = new DataService();

    state = {
        records: [],
        activeId: null,
        activeRecord: {},
        activeIndex: null,
    };

    componentDidMount() {
        this.loadRecords();
    };

    loadRecords = () => {
        this.dataService.getAllRecords()
            .then(this.onRecordsLoaded)
            .catch(this.onError);
    };

    onError = (msg) => {
        console.warn(`Loading error: ${msg}`)
    };

    onRecordsLoaded = (records) => {
        this.setState({
            records: records,
            activeId: records[0] && records[0].id,
            activeRecord: records[0],
            activeIndex: 0,
        });
    };

    getRecordIndexById = (id) => {
        return this.state.records.findIndex(record => record.id === id);
    };

    onActivateMenuItem = (record) => {
        this.setState({
            activeId: record.id,
            activeRecord: record,
            activeIndex: this.getRecordIndexById(record.id),
        });
    };

    onDeleteRecord = () => {
        this.dataService.deleteRecordByIndex(this.state.activeIndex)
            .then(this.loadRecords.bind(this))
            .catch(this.onError);
    };

    onEditRecord = (e) => {
        const { activeRecord, records, activeIndex } = this.state;
        const updatedRecord = Object.assign(activeRecord, {title: e.target.value});
        const updatedRecords = Object.assign([], records, {[activeIndex]: updatedRecord});

        this.setState({
            records: updatedRecords,
        });
    };

    changeItem = (index) => {
        const { records } = this.state;
        const nextRecord = records[index];

        if(nextRecord) {
            this.onActivateMenuItem(nextRecord);
        }
    };

    nextItem = () => {
        this.changeItem(this.state.activeIndex+1);
    };

    prevItem = () => {
        this.changeItem(this.state.activeIndex-1);
    };

    render () {
        const {records, activeId, activeRecord} = this.state;

        return (
            <div className="container align-self-center">
                <div className="row m-1">
                    <div className="col-12 p-0">
                        <ControlsBar onNext={this.nextItem} onPrev={this.prevItem} onDelete={this.onDeleteRecord}/>
                    </div>
                </div>
                <div className="row m-1">
                    <div className="col-md-7 p-0"><Record record={activeRecord} onEdit={this.onEditRecord}/></div>
                    <div className="col-md-5 p-0"><Menu records={records} activeId={activeId} onActivate={this.onActivateMenuItem}/></div>
                </div>
            </div>
        );
    }
}