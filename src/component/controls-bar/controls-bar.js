import React from 'react';

import './controls-bar.css';

const ControlsBar = (props) => {
    const { onPrev, onNext, onDelete} = props;

    return (
        <div className="controls-bar">
            <button className="btn btn-primary" onClick={onPrev}>Prev</button>
            <button className="btn btn-primary" onClick={onNext}>Next</button>
            <button className="btn btn-outline-danger float-right" onClick={onDelete}>Delete</button>
        </div>
    );
};

export default ControlsBar;