import React, { Fragment } from 'react';
import Header from '../header/header';
import Content from '../content/content';

import './app.css';

const App = () => {
    return (
        <Fragment>
            <Header />
            <Content />
        </Fragment>
    );
};

export default App;