import React from 'react';

import './header.css';
import logo from '../../resource/logo.png';

const Header = () => {
    return (
        <header className="container-fluid border-bottom shadow-sm header">
            <div className="container d-flex flex-column flex-md-row align-items-center">
                <h5 className="my-0 mr-md-auto font-weight-bold">
                    <a className="mr-2" href="{}">
                        <img src={logo} className="logo-img p-2" alt="Hamstagram logo" />
                    </a>
                    Hamstagram
                </h5>

                <button className="btn btn-outline-success" href="{}">Upload</button>
                <button className="btn btn-outline-primary" href="{}">Sign up</button>
            </div>
        </header>
    );
};

export default Header;