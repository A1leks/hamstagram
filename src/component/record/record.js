import React from 'react';

import './record.css';

const Record = (props) => {
    const { record: {img = null, title = null, description = null}, onEdit} = props;

    return (
        <div className="record">
            <div className="">
                <form>
                    <h2 className="record-title">
                        <input value={title || ''} onChange={onEdit} />
                    </h2>
                </form>
                <img src={img} alt={title} className="record-img"/>
                <p className="record-description">{description}</p>
            </div>
        </div>
    );
};

export default Record;